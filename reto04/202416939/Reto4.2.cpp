#include <iostream>
using namespace std;

int** Matriz(int n, int m) {

    int** matriz = new int* [n];
    for (int i = 0; i < n; ++i) {
        matriz[i] = new int[m];
        for (int j = 0; j < m; ++j) {
            matriz[i][j] = 0;
        }
    }

    return matriz;
}

void mostrarMatriz(int** matriz, int n, int m) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cout << matriz[i][j] << " ";
        }
        cout << endl;
    }
}

int main() {
    int f, m;
    cout << "Ingrese numero de filas: ";
    cin >> f;
    cout << "Ingrese numero de columnas: ";
    cin >> m;

    int** matriz = Matriz(f, m);


cout << "La matriz con ceros es:" << endl;
 mostrarMatriz(matriz, f, m);


    for (int i = 0; i < f; ++i) {
        delete[] matriz[i];
    }
    delete[] matriz;

    return 0;
}
